# -*- coding: utf-8 -*-

"""
investec.client
~~~~~~~~~~~~~~
This module contains the InvestecBankingClient class responsible for 
communicating with the Investec API.
"""

import datetime
import json
from requests.exceptions import HTTPError
from requests.auth import AuthBase, HTTPBasicAuth
from requests import Request, Session


class InvestecBankingClient(object):
    """
    This class provides an interface for working with the Investec Programmable Banking APIs.

    >>> client = investec.InvestecBankingClient("Your Client ID", "Your Secret")
    """

    def __init__(self, client_id: str, secret: str, timeout: int=30):
        """
        Create a client instance with the provided options.

        :param client_id: Client ID obtained from the IInvestec Programmable Banking website
        :param secret: Secret obtained from the Investec Programmable Banking website
        :param timeout: The timeout for API requests in seconds
        """

        if not client_id or not secret:
            raise ValueError("Both the client_id and secret are required.")
        self.client_id = client_id
        self.secret = secret
        self.timeout = timeout
        self.session = Session()
        self.base_url = "https://openapi.investec.com"
        self.token_expiration_timestamp = datetime.datetime.now()
        self.token = None


    def _call_api(self, resource_path: str, http_method: str="get", params: dict=None, body: str=None, content_type: str=None) -> dict:
        """
        Helper function to call the API

        :param resource_path: The path to the resource
        :param http_method: The HTTP verb
        :param params: The query string parameters for the request
        :param body: The payload for the request
        :returns: The API response
        :raises HTTPError: In the event the response is not valid
        """

        # Get the authoriser
        auth = self._get_auth(resource_path)

        # Set default headers
        headers = {"Accept": "application/json"}

        # Add the additional Content-Type header if specified, or default it
        # if this is a POST request.
        http_method = str.lower(http_method)
        if content_type:
            headers["Content-Type"] = content_type
        elif http_method == "post":
            headers["Content-Type"] = "application/x-www-form-urlencoded; charset=utf-8"
        
        # Execute request depending on provided http method
        response = getattr(self.session, http_method)(
            f"{self.base_url}/{resource_path}",
            params=params,
            data=body,
            headers=headers,
            auth=auth,
            timeout=self.timeout
        )

        # Retrieve the content if successful, otherwise raise an HTTPError
        try:
            response.raise_for_status()
            content = response.json()
            if "data" in content:
                return content["data"]
            return content
        except:
            raise HTTPError(response.status_code, response.content)


    def _get_auth(self, resource_path) -> AuthBase:
        """Get an authoriser object depending on the resource path and 
        refresh token if required."""
        
        if resource_path.endswith("/token"):
            # If retrieving a token, use basic auth
            return HTTPBasicAuth(self.client_id, self.secret)
        else:
            # Otherwise, refresh the token if necessary and use bearer auth
            if not self.token or datetime.datetime.now() >= self.token_expiration_timestamp:
                self._refresh_access_token()
            
            return HTTPBearerAuth(self.token)


    def _refresh_access_token(self) -> None:
        """Refresh the OAuth access token"""

        # Request a new token from the API
        response = self._call_api(
            "identity/v2/oauth2/token",
            http_method="post",
            body="grant_type=client_credentials&scope=accounts"
        )
        self.token = response["access_token"]

        # Expire the token 5 minutes before it actually expires
        token_expiry_seconds = response["expires_in"] - (5 * 60)
        self.token_expiration_timestamp = datetime.datetime.now() + datetime.timedelta(seconds=token_expiry_seconds)


    def get_accounts(self) -> dict:
        """
        Gets the available accounts

        :returns: The accounts
        """
        return self._call_api(
            "za/pb/v1/accounts"
        )["accounts"]


    def get_account_balance(self, account_id: str) -> dict:
        """
        Gets the balance for an account

        :param account_id: The account to get the balance for
        :returns: The balance information
        """
        return self._call_api(
            f"za/pb/v1/accounts/{account_id}/balance"
        )


    def get_account_transactions(self, account_id: str, from_date: datetime=None, to_date: datetime=None, transaction_type: str=None) -> dict:
        """
        Gets the transactions for an account.

        :param account_id: The account to get the transactions for
        :param from_date: Earliest date for which transactions should be retrieved (inclusive)
        :param to_date: Latest date for which transactions should be retrieved (inclusive)
        :param transaction_type: Transaction type string to filter on
        :returns: The transactions
        """
        if from_date and to_date and to_date < from_date:
            raise ValueError("The from_date must be before the to_date")

        params = {}
        if from_date:
            params["fromDate"] = from_date.strftime("%Y-%m-%d")
        if to_date:
            params["toDate"] = to_date.strftime("%Y-%m-%d")
        if transaction_type:
            params["transactionType"] = transaction_type
        
        return self._call_api(
            f"za/pb/v1/accounts/{account_id}/transactions",
            params=params
        )["transactions"]


    def get_cards(self) -> dict:
        """
        Gets the available cards

        :returns: The cards
        """
        return self._call_api(
            "za/v1/cards"
        )["cards"]

    
    def get_card_saved_code(self, card_key: str) -> dict:
        """
        Gets the saved code for a card

        :param card_key: The card to get the saved code for
        :returns: The saved code for the card
        """
        return self._call_api(
            f"/za/v1/cards/{card_key}/code"
        )["result"]


    def get_card_published_code(self, card_key: str) -> dict:
        """
        Gets the published code for a card

        :param card_key: The card to get the published code for
        :returns: The published code for the card
        """
        return self._call_api(
            f"/za/v1/cards/{card_key}/publishedcode"
        )["result"]


    def update_card_code(self, card_key: str, code: str) -> dict:
        """
        Updates the code for a card without publishing

        :param card_key: The card to which the code should be saved
        :param code: The code to save to the card
        :returns: The result
        """
        return self._call_api(
            f"/za/v1/cards/{card_key}/code",
            http_method="post",
            body=json.dumps({"code": code}),
            content_type="application/json"
        )["result"]


    def publish_code_to_card(self, card_key: str, code_id: str) -> dict:
        """
        Publishes the saved code with the specified id to the specified card

        :param card_key: The card to which the code should be saved
        :param code_id: The id of the code that should be published
        :returns: The result
        """
        return self._call_api(
            f"/za/v1/cards/{card_key}/publish",
            http_method="post",
            body=json.dumps({"codeid": code_id, "code": " "}),
            content_type="application/json"
        )["result"]


    def get_card_code_executions(self, card_key: str) -> dict:
        """
        Gets the code executions for a specified card

        :param card_key: The card to get the executions for
        :returns: The code executions for the card
        """
        return self._call_api(
            f"/za/v1/cards/{card_key}/code/executions"
        )["result"]


    def get_card_environment(self, card_key: str) -> dict:
        """
        Gets the environment variables for a specified card

        :param card_key: The card to get the environment variables for
        :returns: The environment for the card
        """
        return self._call_api(
            f"/za/v1/cards/{card_key}/environmentvariables"
        )["result"]


    def replace_card_environment(self, card_key: str, variables: dict) -> dict:
        """
        Replaces the environment variables for a specified card

        :param card_key: The card for which the environment should be replaced
        :param variables: A dict representation of the new environment
        :returns: The result
        """
        return self._call_api(
            f"/za/v1/cards/{card_key}/environmentvariables",
            http_method="post",
            body=json.dumps({"variables": variables}),
            content_type="application/json"
        )["result"]


    def get_countries(self) -> dict:
        """
        Gets the reference set of transaction countries

        :returns: The reference set of transaction countries
        """
        return self._call_api(
            "/za/v1/cards/countries"
        )["result"]


    def get_currencies(self) -> dict:
        """
        Gets the reference set of transaction currencies

        :returns: The reference set of transaction currencies
        """
        return self._call_api(
            "/za/v1/cards/currencies"
        )["result"]


    def get_merchants(self) -> dict:
        """
        Gets the reference set of merchants

        :returns: The reference set of merchants
        """
        return self._call_api(
            "/za/v1/cards/merchants"
        )["result"]


    def transfer_between_accounts(self, source_account_id: str, destination_account_id: str, rand_value: float, source_ref: str, destination_ref: str) -> dict:
        """
        Transfers a specified rand value from a source account to a destination account,
        with the specified reference strings.

        :param source_account_id: The account from which money should be transferred
        :param destination_account_id: The account to which money should be transferred
        :param rand_value: The value in ZAR to be transferred
        :param source_ref: Reference string for the source account
        :param destination_ref: Reference string for the destination account
        :returns: The result
        """

        body = {
            "AccountId": source_account_id,
            "TransferList": [
                {
                    "BeneficiaryAccountId": destination_account_id,
                    "Amount": rand_value,
                    "MyReference": source_ref,
                    "TheirReference": destination_ref
                }
            ]
        }

        return self._call_api(
            "/za/pb/v1/accounts/transfermultiple",
            http_method="post",
            body=json.dumps(body),
            content_type="application/json"
        )["transferResponse"]


class HTTPBearerAuth(AuthBase):
    """Helper class to add Bearer Auth to requests"""

    def __init__(self, token: str):
        """
        Initialise with the token

        :param token: The bearer token
        """
        self.token = token


    def __call__(self, r: Request) -> Request:
        """
        Sets the correct header for the request

        :param r: The request
        :returns: The modified request
        """
        r.headers["Authorization"] = "Bearer " + self.token
        return r
