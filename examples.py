import os
import random
from investec.client import InvestecBankingClient

###################
#      SETUP      #
###################
# First, we retrieve creds from the environment
client_id = os.getenv("INVESTEC_CLIENT_ID")
secret = os.getenv("INVESTEC_SECRET")

# Then, we initialise the client
client = InvestecBankingClient(client_id, secret)

###################
# READ OPERATIONS #
###################
# Example 1 => Get accounts
print("\n== Example 1: get_accounts ==")
accounts = client.get_accounts()
print(f"Total number of accounts: {len(accounts)}")
print(f"Random account: {accounts[random.randint(0, len(accounts)-1)]}")

# Example 2 => Get account balance
print("\n== Example 2: get_account_balance ==")
balance = client.get_account_balance(accounts[0]["accountId"])
print(f"Current balance for account '{accounts[0]['accountId']}': {balance['currentBalance']}")
print(f"Available balance for account '{accounts[0]['accountId']}': {balance['availableBalance']}")

# Example 3 => Get account transactions
print("\n== Example 3: get_account_transactions ==")
transactions = client.get_account_transactions(accounts[0]["accountId"])
print(f"Total number of transactions for account '{accounts[0]['accountId']}': {len(transactions)}")
print(f"Random transaction: {transactions[random.randint(0, len(transactions)-1)]}")

# Example 4 => Get cards
print("\n== Example 4: get_cards ==")
cards = client.get_cards()
print(f"Total number of cards: {len(cards)}")
print(f"Random card: {cards[random.randint(0, len(cards)-1)]}")

# Example 5 => Get saved code for card
print("\n== Example 5: get_card_saved_code ==")
saved_code = client.get_card_saved_code(cards[0]["CardKey"])
print(f"Saved codeId for card with key '{cards[0]['CardKey']}': {saved_code['codeId']}")

# Example 6 => Get published code for card
print("\n== Example 6: get_card_published_code ==")
published_code = client.get_card_published_code(cards[0]["CardKey"])
print(f"Published codeId for card with key '{cards[0]['CardKey']}': {published_code['codeId']}")

# Example 7 => Get code executions for card
print("\n== Example 7: get_card_code_executions ==")
executions = client.get_card_code_executions(cards[0]["CardKey"])["executionItems"]
print(f"Total number of executions for card with key '{cards[0]['CardKey']}': {len(executions)}")
print(f"Random execution: {executions[random.randint(0, len(executions)-1)]}")

# Example 8 => Get environment for card
print("\n== Example 8: get_card_environment ==")
environment = client.get_card_environment(cards[0]["CardKey"])["variables"]
print(f"Environment for card with key '{cards[0]['CardKey']}': {environment}")

# Example 9 => Get reference list of countries
print("\n== Example 9: get_countries ==")
countries = client.get_countries()
print(f"Total number of countries: {len(countries)}")
print(f"Random country: {countries[random.randint(0, len(countries)-1)]}")

# Example 10 => Get reference list of currencies
print("\n== Example 10: get_currencies ==")
currencies = client.get_currencies()
print(f"Total number of currencies: {len(currencies)}")
print(f"Random currency: {currencies[random.randint(0, len(currencies)-1)]}")

# Example 11 => Get reference list of merchant type to code mappings
print("\n== Example 11: get_merchants ==")
merchants = client.get_merchants()
print(f"Total number of merchants: {len(merchants)}")
print(f"Random merchant: {merchants[random.randint(0, len(merchants)-1)]}")

####################
# WRITE OPERATIONS #
####################
skip_testing_write_operations = os.getenv("SKIP_TESTING_WRITE_OPERATIONS", 'True').lower() not in ('false', '0', 'f')
print(f"\n\nSKIP_TESTING_WRITE_OPERATIONS retrieved from environment with value: {skip_testing_write_operations}")

if not skip_testing_write_operations:
    print("Running through write operation examples...")

    # Example 12 => Update saved code for card
    print("\n\n== Example 12: update_card_code ==")
    update_saved_code_result = client.update_card_code(cards[0]["CardKey"], saved_code["code"])
    print(f"Saved codeId: {update_saved_code_result['codeId']}")

    # Example 13 => Replace environment for card
    print("\n== Example 13: replace_card_environment ==")
    replace_environment_result = client.replace_card_environment(cards[0]["CardKey"], environment)
    print(f"New environment: {replace_environment_result['variables']}")

    # Example 14 => Publish code for card
    print("\n== Example 14: publish_code_to_card ==")
    publish_code_result = client.publish_code_to_card(cards[0]["CardKey"], update_saved_code_result["codeId"])
    print(f"Published codeId: {publish_code_result['codeId']}")
else:
    print("Skipping testing of write operations...")

#######################
# TRANSFER OPERATIONS #
#######################
skip_testing_transfer_operations = os.getenv("SKIP_TESTING_TRANSFER_OPERATIONS", 'True').lower() not in ('false', '0', 'f')
print(f"\n\nSKIP_TESTING_TRANSFER_OPERATIONS retrieved from environment with value: {skip_testing_transfer_operations}")

if not skip_testing_transfer_operations:
    print("Running through transfer operation examples...")

    # Example 15 => Transfer from one account to another
    print("\n\n== Example 15: transfer_between_accounts ==")
    transfer_between_accounts_result = client.transfer_between_accounts(
        accounts[0]["accountId"],
        accounts[1]["accountId"],
        0.10,
        "Test transfer source",
        "Test transfer destination"
    )
    print(f"Result: {transfer_between_accounts_result}")
else:
    print("Skipping testing of transfer operations...")

print("\n\nAll done!")
