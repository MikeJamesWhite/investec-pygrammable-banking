# Python Wrapper for Investec Programmable Banking Open API

This is my fork of the [investec-openbanking-python](https://gitlab.com/vchegwidden/investec-openbanking-python) repository. I'm aiming to expand upon the functionality provided there, by supporting both the [Programmable Cards API](https://developer.investec.com/programmable-banking/#open-api-programmable-cards-api) and upcoming Transfer API.

## Investec Client
Currently, [the client](investec/client.py) supports:
- [Accounts API](https://developer.investec.com/programmable-banking/#open-api-accounts)
- [Programmable Cards API](https://developer.investec.com/programmable-banking/#open-api-programmable-cards-api)
- Transfer API (Beta)
- Managed token state, with automatic refresh when close to expiry, using the [Authorization API](https://developer.investec.com/programmable-banking/#open-api-authorization)

All responses are returned from the client in the form of dictionaries.

## Getting started

### Virtual environment
Create a virtual environment:

On macOS and Linux: `python3 -m venv .venv`\
On Windows: `py -m venv .venv`

Then install the requirements:

`.venv/bin/python -m pip install -r requirements.txt`

### Docker
Alternatively, a Dockerfile and docker-compose.yml file are included.

With docker-compose installed, running the examples is as simple as adding your credentials to the environment in docker-compose.yml and then running:

`docker-compose build && docker-compose up`

## Usage
A quickstart example is shown below, but there is also an [examples.py](examples.py) file provided that aims to demonstrate the usage of all supported methods.

```Python
    # Create instance of the client, get a list of the accounts and print the size of the list
    from investec.client import InvestecBankingClient

    client_id = "YOUR CLIENT ID"
    secret = "YOUR SECRET"

    client = InvestecBankingClient(client_id, secret)

    accounts = client.get_accounts()
    balance = client.get_account_balance(accounts[0]["accountId"])
    transactions = client.get_account_transactions(accounts[0]["accountId"])

    print("accounts = %d" % len(accounts))
    print("balance = %d" % balance["availableBalance"])
    print("transactions = %d" % len(transactions))
```
